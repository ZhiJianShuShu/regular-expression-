﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegExStudy
{
    /// <summary>
    /// 使用元字符
    /// </summary>
    public class Chapter4
    {
        /// <summary>
        /// 对特殊字符进行转义
        /// </summary>
        public void Check1()
        {
            string str = @"  
            int[] numArry = new int[5];
            for (int i = 0; i < numArry.Length; i++)   
            {
                Console.WriteLine(numArry[0]);
            }
            ";

            // 在这里想要查找 numArry[0] ，但是方括号[] 是个特殊符号，元字符 第三章中有讲到
            // 因为是特殊符号，这个时候就需要转义，用反斜杠 \  
            // 所以有 \[\]  用来查找 []
            // 如果 [0] 内还需要匹配字符 那么，则需要写成  \[[0-9]\]  [0-9] 来表示匹配的下标

            var arry = Regex.Matches(str, @"numArry\[0\]");
            Console.WriteLine(string.Format("Check1 查询 文本 匹配字符 {0} ", str, @"numArry\[0\]"));
            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();
        }

        /// <summary>
        /// 匹配空字符
        /// </summary>
        public void Check2()
        {
            // 元字符大概分两种 一种是用来匹配文本的(比如 . ),另一种则是表达式的语法所要求的
            // 空白元字符   [\b]   \f  \n  \r  \t  \v
        }

        /// <summary>
        /// 匹配任意的字符类别
        /// </summary>
        public void Check3()
        {
            // 匹配数字
            // \d 等价于 [0-9]   \D 等价于 [^0-9]

            // 匹配字母和数字
            // \w 等价于 [a-zA-Z0-9]  \W 等价于 [^a-zA-Z0-9]

            // 假如四个字符的匹配规则为  任意字母数字 + 数字 + 任意字母数字 + 数字
            string str = "9527  6666  z1x2  A66X    1D2C    W1q4  ";

            var arry = Regex.Matches(str, @"\w\d\w\d");
            Console.WriteLine(string.Format("Check3 查询 文本 \n {0} \n匹配字符 {1} ", str, @"\w\d\w\d"));
            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();

            // 匹配空白字符
            // \s 等价于 [\f\n\r\t\v]  \S 等价于 [^\f\n\r\t\v]

            // 匹配十六进制或者八进制数值
            // 十六进制 需要用 \x 来表示  比如  \x0A 对应 ASCII 字符10 等价于 \n
            // 晚点补例子

            // 八进制 需要用 \0 来表示  比如  \x11 对应 ASCII 字符9 等价于 \t
            // 晚点补例子
        }

        /// <summary>
        /// 使用 POSIX 字符类
        /// </summary>
        public void Check4()
        {
            // POSIX 字符 和前面学的可以替换使用，具体可以看书
        }
    }
}
