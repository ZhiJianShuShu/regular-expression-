﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegExStudy
{
    /// <summary>
    /// 嵌入条件
    /// </summary>
    public class Chapter10
    {
        // 强大但却不常用的功能，在表达式的内部嵌入条件处理功能


        public void Check1()
        {
            // 号码格式匹配
            // (123)456-7890 和 123-456-7890 都是可接受的格式

            string str = "123-456-7890  \n  (123)456-7890 \n  (123)-456-7890  \n  (123-456-7890  \n  1234567890";

            var arry = Regex.Matches(str, @"\(?\d{3}\)?-?\d{3}-\d{4}");
            Console.WriteLine(string.Format("Check7 查询 文本 \n {0} \n匹配字符 {1} ", str, @"\(?\b{3}\)?-?\b{3}-\b{4}"));
            // 但实际上 第三第四个没有识别到,这个时候需要用到条件处理来判断
            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();
        }

        //正则表达里的条件要用  ?  来定义
        //根据回溯引用 前后查找  进行条件处理

        /// <summary>
        ///  //回溯引用条件
        /// </summary>
        public void Check2()
        {
            //只在一个前面的子表达式搜索取得成功的情况下才允许使用一个表达式
            string str = "123-456-7890  \n  (123)456-7890 \n  (123)-456-7890  \n  (123-456-7890  \n  1234567890";

            // (\()?\d{3}(?(1)\)|-)
            // (\()? -> 表达式 \( 查找是否有左侧括号
            // (?(1)\)|-)  ->  ?(1)\) ->  ?(1)  表示 满是第1个表达式之后才判断 \)   令() 配对时才匹配
            // ?(index) 表示满足对应下标的表达式时 才执行 ?(index) 后的判断匹配


            var arry = Regex.Matches(str, @"(\()?\d{3}(?(1)\)|-)\d{3}-\d{4}");
            Console.WriteLine(string.Format("Check7 查询 文本 \n {0} \n匹配字符 {1} ", str, @"(\()?\d{3}(?(1)\)|-)\d{3}-\d{4}"));
            Console.WriteLine("匹配的数量 ： " + arry.Count);
            // 输出有些出入 (123-456-7890 的 ( 检测被忽略了，感觉还要补充处理才行
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();
        }

        /// <summary>
        /// 前后查找条件
        /// </summary>
        public void Check3()
        {
            // 只在一个向前查找或向后查找成功是，才允许一个表达式被使用
            // 举例匹配美国邮政编码  
            // 12345  或 12345-6789   五位数学-四位数字

            string str = "11111 \n 22222 \n 33333- \n 44444-4444";

            // (?(?=-)-\d{4})   ->  (?=-)   这个子表达中向前查找，是否有  -
            // ?(?=-)  这个条件满足后，再匹配后面的条件

            var arry = Regex.Matches(str, @"\d{5}(?(?=-)-\d{4})");
            Console.WriteLine(string.Format("Check7 查询 文本 \n {0} \n匹配字符 {1} ", str, @"\d{5}(?(?=-)-\d{4})"));

            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();
        }
    }
}
