﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegExStudy
{
    /// <summary>
    /// 匹配一组字符
    /// </summary>
    public class Chapter3
    {
        /// <summary>
        /// 匹配多个字符中的一个 [] 的使用
        /// </summary>
        public void Check1()
        {
            //[] 内填入的字符，表示需要匹配的字符
            // [nu]a.\.xls  表示 a 前面必须是 n 或者 u

            string str = "na1.xls \n sa1.xls \n uap.xls \n lao.xls \n ";

            var arry = Regex.Matches(str, @"[nu]a.\.xls");
            Console.WriteLine(string.Format("Check1 查询 文本 \n {0} \n匹配字符 {1} ", str, @"[nu]a.\.xls"));
            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();
        }

        /// <summary>
        /// 利用字符几何区间 依旧是  [] 的使用(元字符)
        /// </summary>
        public void Check2()
        {
            //[A-Z] 表示A-Z的所有大写 [a-z] 同理
            //[0-9] 同理
            //当然也可以都写在一起 ，也可以填入具体字符 [A-Za-z0-9]
            // [a-z]a[0-9]\.xls  a 前面必须是小写 a-z 且后面必须是数字，和上面的Check1 查询就不太一样了
            string str = "na1.xls \n sa1.xls \n uap.xls \n lao.xls \n ";

            var arry = Regex.Matches(str, @"[a-z]a[0-9]\.xls");
            Console.WriteLine(string.Format("Check2 查询 文本 \n {0} \n匹配字符 {1} ", str, @"[a-z]a[0-9]\.xls"));
            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();
        }

        /// <summary>
        /// 取非匹配 [^]  
        /// </summary>
        public void Check3()
        {
            // 在[] 内填入 ^  那么就是指不匹配里面的字符
            // [a-z]a[^0-9]\.xls   和2 不一样的是，a 后面不能有数字
            string str = "na1.xls \n sa1.xls \n uap.xls \n lao.xls \n ";

            var arry = Regex.Matches(str, @"[a-z]a[^0-9]\.xls");
            Console.WriteLine(string.Format("Check3 查询 文本 \n {0} \n匹配字符 {1} ", str, @"[a-z]a[^0-9]\.xls"));
            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();
        }

    }
}
