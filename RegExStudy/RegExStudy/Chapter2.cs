﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegExStudy
{
    /// <summary>
    /// 匹配单个字符
    /// </summary>
    public class Chapter2
    {
        /// <summary>
        /// 使用字符匹配字符
        /// </summary>
        public void CheckStr1()
        {
            //匹配任意字符
            string str = "hello, my name is Ben. Please visit my website xxx My ";

            // 这里做了全字符匹配，区分了大小写，如何区分大小写在后面章节会提
            var arry = Regex.Matches(str,"my");
            Console.WriteLine(string.Format("查询 文本 {0} \n匹配字符 {1} ", str,"my"));
            Console.WriteLine(" my 出现的次数： " + arry.Count);
        }

        /// <summary>
        ///  匹配任意字符  使用符号点 ->  .
        /// </summary>
        public void CheckStr2()
        {
            // . 可以匹配字符后的任意字符，包括自己 .
            Console.WriteLine();

            string str = "sales.xls \n  sales1.xls \n order.xls \n sales2.xls \n apac1.xls \n na1.xls \n usales.xls";

            var arry = Regex.Matches(str, "sales.");

            Console.WriteLine(string.Format("匹配任意字符 \n {0} \n匹配字符 {1} ", str, "sales."));
            Console.WriteLine(" sales. 出现的次数： " + arry.Count);


            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ",item.ToString()));
            }
        }

        /// <summary>
        /// 匹配特殊字符  \  反斜杠，也称为 元字符 
        /// </summary>
        public void CheckStr3()
        {
            // \ 用于转义，跟在它后面的是一个特殊的字符

            Console.WriteLine();

            string str = "sales.xls \n  sales1.xls \n order.xls \n sales2.xls \n apac1.xls \n na1.xls \n usales.xls";

            // 在 \ 后的 . 这个时候特指是符号，而不是任意一个数
            var arry = Regex.Matches(str, @"sales\.");

            Console.WriteLine(string.Format("匹配特殊字符 \n {0} \n匹配字符 {1} ", str, @"sales\."));
            Console.WriteLine(" sales. 出现的次数： " + arry.Count);

            
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
        }
    }
}
