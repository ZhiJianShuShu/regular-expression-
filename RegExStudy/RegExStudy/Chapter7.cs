﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegExStudy
{
    /// <summary>
    /// 使用子表达式
    /// </summary>
    public class Chapter7
    {
        /// <summary>
        /// 子表达式 () 小括号的使用
        /// </summary>
        public void Check1()
        {
            // 例如 ip 地址的匹配
            string str = "Pinging hog.forta.com 192.168.1.1 ";

            // \d{1,3}\.{3}\b{1,3}  开始或许会想用这个匹配数量的方法，但是不行。 {n} 只对前面的内容匹配对应的次数，这样的话匹配的是 \. 
            // 所以我们需要把想要的内容包裹起来 (\d{1,3}\.)  整个包裹起来的整体就叫做一个 子表达式

            var arry = Regex.Matches(str, @"(\d{1,3}\.){3}\b{1,3}");
            Console.WriteLine(string.Format("Check1 查询 文本 \n {0} \n匹配字符 {1} ", str, @"(\d{1,3}\.){3}\b{1,3}"));

            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();

            // |  或的使用
            // 匹配 19xx  20xx 年
            str = "1901  1995  2001  2077  3101  2100   ";

            arry = Regex.Matches(str, @"(19|20)\d{2}");
            Console.WriteLine(string.Format("Check1 查询 文本 \n {0} \n匹配字符 {1} ", str, @"(19|20)\d{2}"));

            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();
        }

        /// <summary>
        /// 子表达式的嵌套
        /// </summary>
        public void Check2()
        {
            // 取上面的ip地址做例
            // \d{1,3}\.{3}\b{1,3} 能够匹配ip 地址，但是却无法限制地址的有效性

            // 满足条件
            // 任何一个1位或2位数字 0-99                                    (\d{1,2})
            // 任何一个以1开头的3位数字 100-199                             (1\d{2})
            // 任何一个以2开头、第2位数字在 0~4 之间的三位数  200 - 249     (2[0-4]\d)
            // 任何一个以25开头、第3位数字在 0~5 之间的3位数 250~255        (25[0-5])

            string str = " 192.168.1.1 \n  192.168.255.256  \n  127.0.0.1  \n  98.300.0.5 ";

            var arry = Regex.Matches(str, @"(((\d{1,2})|(1\d{2})|(2[0-4]\d)|(25[0-5]))\.){3}((\d{1,2})|(1\d{2})|(2[0-4]\d)|(25[0-5]))");
            Console.WriteLine(string.Format("Check1 查询 文本 \n {0} \n匹配字符 {1} ", str, @"(((\d{1,2})|(1\d{2})|(2[0-4]\d)|(25[0-5]))\.){3}((\d{1,2})|(1\d{2})|(2[0-4]\d)|(25[0-5]))"));
            //  192.168.255.256 给匹配进去了 256 把 6 裁掉了。em 想想
            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();
        }
    }
}
