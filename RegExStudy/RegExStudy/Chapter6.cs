﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegExStudy
{
    /// <summary>
    /// 位置匹配
    /// </summary>
    public class Chapter6
    {
        /// <summary>
        /// 边界 \b 的使用
        /// </summary>
        public void Check1()
        {
            // \b  表示一个空格的占位符 用来和前后字符做分隔区分

            string str = "The captain wore his cap and cape produly as he sat listening to the recap of how his crew saved the men form a capsized vessel.";
            //这里 想要查找 cap 这个完整的单词
            var arry = Regex.Matches(str, @"\bcap\b");
            Console.WriteLine(string.Format("Check1 查询 文本 \n {0} \n匹配字符 {1} ", str, @"b\cap\b"));

            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();
        }

        /// <summary>
        /// 字符串边界  ^\s*   和  \s*$  的使用
        /// </summary>
        public void Check2()
        {
            // ^ 在前面的 [^] 表示非的字符匹配，在这里  ^\s*   匹配一个字符串的开头位置
            // 假如 html 的匹配

            string str1 = "<?xml version=\"1.0\" encoding=\"UTF-8\" ? > xml.....   <img>....<img>";
            string str2 = "fhdjskajf  <?xml version=\"1.0\" encoding=\"UTF-8\" ? > xml.....   <img>....<img>";

            var arry = Regex.Matches(str1, @"^\s*<\?xml.*?>");
            Console.WriteLine(string.Format("Check2  查询 str1 文本 \n {0} \n匹配字符 {1} ", str1, @"^\s*<\?xml.*?>"));
            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();

            arry = Regex.Matches(str2, @"^\s*<\?xml.*?>");
            Console.WriteLine(string.Format("Check2  查询 str2 文本 \n {0} \n匹配字符 {1} ", str2, @"^\s*<\?xml.*?>"));
            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }

            Console.WriteLine();

            // \s*$  检测结尾不应该还有其他内容
            // 检测一份 html 后是否还有其他内容
            string str3 = "</html> fdgfdss";
            string str4 = "fdgfdss</html> ";

            // 这里满足后面不包含内容的时候应该是返回1，即符合要求 返回最后是</html> 这个字符串做结尾
            arry = Regex.Matches(str3, @"</[Hh][Tt][Mm][Ll]>\s*$");
            Console.WriteLine(string.Format("Check2  查询 str3 文本 \n {0} \n匹配字符 {1} ", str3, @"</[Hh][Tt][Mm][Ll]>\s*$"));
            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();


            arry = Regex.Matches(str4, @"</[Hh][Tt][Mm][Ll]>\s*$");
            Console.WriteLine(string.Format("Check2  查询 str4 文本 \n {0} \n匹配字符 {1} ", str4, @"</[Hh][Tt][Mm][Ll]>\s*$"));
            Console.WriteLine("匹配的数量 ： " + arry.Count);
            foreach (var item in arry)
            {
                Console.WriteLine(string.Format("分别为 {0} ", item.ToString()));
            }
            Console.WriteLine();

        }
    }
}
